import { Typography } from '@mui/material';
import './App.css';
import DataTableUser from './component/DatatableUser';

function App() {
  return (
    <div>
      <Typography align='center' variant='h4'>Danh sách người dùng đăng ký</Typography>
      <DataTableUser></DataTableUser>
    </div>
  );
}

export default App;
