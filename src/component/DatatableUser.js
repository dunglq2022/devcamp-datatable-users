import { Button, Container, Box, Typography, Modal, TablePagination, ButtonGroup, TextField, MenuItem, Alert, Snackbar, AlertTitle } from "@mui/material";
import { styled } from '@mui/material/styles';
import {Table, TableHead, TableRow, TableBody, TableContainer, Paper, TableCell, tableCellClasses } from '@mui/material/';
import { blue, green, grey, red } from "@mui/material/colors";
import { useState, useEffect } from "react";

//==========QUẢN LÝ CÁC COMPONENT-STYLED============
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // Kẻ viền cho bảng
    '& td, & th': {
        border: `1px solid ${grey[300]}`
    }
  }));

  const StyledTableCell = styled(TableCell)(() => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: grey[0],
        color: grey[900],
        fontWeight:'bold',
        border: `1px solid ${grey[300]}`,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,      
    },
    }));

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };
//==========END-QUẢN LÝ CÁC COMPONENT-STYLED============


function DataTableUser () {
//========== QUẢN LÝ CÁC ARRAY ============    
    const countriesArr = [
        {
            value: 'VN',
            label: 'Viet Nam'
        },
        {
            value: 'US',
            label: 'United States'
        },
        {
            value: 'CAN',
            label: 'Canada'
        },
        {
            value: 'AUS',
            label: 'Australia'
        }
    ] 

    const customerTypeArr = [
        {
            value: 'Gold',
            label: 'Gold'
        },
        {
            value: 'Premium',
            label: 'Premium'
        },
        {
            value: 'Standard',
            label: 'Standard'
        }
    ]

    const registerStatusArr = [
        {
            value: 'Accepted',
            label: 'Accepted'
        },
        {
            value: 'Denied',
            label: 'Denied'
        },
        {
            value: 'Standard',
            label: 'Standard'
        },
        {
            value: 'New',
            label: 'New'
        }
    ]
//========== END-QUẢN LÝ CÁC ARRAY ============   

    //=============QUẢN LÝ STATE=================//
    //Thông tin xử lý arr R-Read
    const [rows, setRows] = useState([]);
    const [selectRow, setSelectRow] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    //Thông tin xử lý button sửa U-Update
    const [openEdit, setOpenEdit] = useState(false);
    const handleCloseEdit = () => setOpenEdit(false);
    //Thông tin xử lý button thêm C-Create
    const [openCreate, setOpenCreate] = useState(false);
    const handleCloseCreate = () => setOpenCreate(false);
    //Thông tin xử lý button xóa D-Delete
    const [openDelete, setOpenDelete] = useState(false);
    const handleCloseDelete = () => setOpenDelete(false);
    //Thông tin xử lý các trường trong thêm user
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [subject, setSubject] = useState('');
    const [country, setCountry] = useState('');
    const [open, setOpen] = useState(false);
    const [message, setMsessage] = useState('')
    const [severity, setSeverity] = useState('')
    
    //=============END-QUẢN LÝ STATE=================//

    //=============QUẢN LÝ API=================//  
    //async await API
    const fetchAPI = async(url, requestOptions) => {
        const response = await fetch(url, requestOptions);
        return response;
    }
    //========== READ - R================
    useEffect(()=>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions)
        .then(response => response.json())
        .then(result => {
            setRows(result);
        }
        )
        .catch(error => console.log('error', error));        
    },[page, rows])
    //=============END-QUẢN LÝ API=================//


    //=============QUẢN LÝ CÁC EVENT===============//
    //Xử lý button tại index trong row console log information từng row
    const handleButtonEdit = (row) => {
        console.log(row)
        setOpenEdit(true)   
        setSelectRow(row)
    };

    const handleButtonDelete = (row) => {
        console.log(row)
        setOpenDelete(true)   
        setSelectRow(row)
    };

    //Xử lý handle change Page
    const handleChangePage = (event, value) => {
        setPage(value)
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const handleAddUser = () => {
        setOpenCreate(true)
    };

    const onChangeFirstName = (event) => {
        console.log(event.target.value);
        setFirstName(event.target.value);
        
    };

    const onChangeLastName = (event) => {
        console.log(event.target.value);
        setLastName(event.target.value);
        
    };
    
    const onChangeSubject = (event) => {
        console.log(event.target.value);
        setSubject(event.target.value);
        
    };

    const onChangeCountry = (value) => {
        console.log(value.target.value);
        setCountry(value.target.value);
        
    };

    const handleConfirmCreate = () => {
        const newUser = JSON.stringify ({
            firstname: firstName,
            lastname: lastName,
            subject: subject,
            country: country,
        })
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'POST',
            body:newUser,
            headers: myHeaders,
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions)
        .then(result => {
            if(result.status === 201){
                setMsessage('Tạo mới thành công user');
                setOpen(true);
                setSeverity('success')
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Tạo user mới không thành công, bạn vui lòng check lại')
            setSeverity('error')
        });
        handleCloseCreate();
    };

    const handleUpdateUser = (paramSelectRow) => {
        const updateUser = JSON.stringify ({
            firstname: paramSelectRow.firstname,
            lastname: paramSelectRow.lastname,
            subject: paramSelectRow.subject,
            country: paramSelectRow.country,
            customerType: paramSelectRow.customerType,
            registerStatus: paramSelectRow.registerStatus
        })
        
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'PUT',
            body:updateUser,
            headers: myHeaders,
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users/" + paramSelectRow.id, requestOptions)
        .then(response => {
            console.log(response);
            if (response.status === 200) {
                setMsessage('Cập nhât thông tin thành công user');
                setOpen(true);
                setSeverity('success')
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Cập nhật user không thành công, bạn vui lòng check lại')
            setSeverity('error')
        });
        handleCloseEdit();
    };

    const handleDeleteUser = (paramSelectRow) => {
        //console.log(paramSelectRow.id)
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
          };
        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users/" + paramSelectRow.id, requestOptions)
        .then(response => {
            console.log(response);
            if (response.status === 204) {
                setMsessage('Xóa thông tin thành công user');
                setOpen(true);
                setSeverity('success')
            }
        })
        .catch(error => {
            console.log('error', error)
            setMsessage('Lỗi - Xóa không thành công, bạn vui lòng check lại')
            setSeverity('error')
        });
        handleCloseDelete();
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    };
//=============END-QUẢN LÝ CÁC EVENT===============//
    return(
        <Container maxWidth={'xl'} sx={{mt: 3}}>
            <Button sx={{
                mb :3
            }} 
            color="success" 
            variant="contained"
            onClick={handleAddUser}
            >Thêm User</Button>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead>
                <TableRow>
                    <StyledTableCell>Mã người dùng</StyledTableCell>
                    <StyledTableCell>Firstname</StyledTableCell>
                    <StyledTableCell>Lastname</StyledTableCell>
                    <StyledTableCell>Country</StyledTableCell>
                    <StyledTableCell>Subject</StyledTableCell>
                    <StyledTableCell>Customer Type</StyledTableCell>
                    <StyledTableCell>Register Status</StyledTableCell>
                    <StyledTableCell>Action</StyledTableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    {rows
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                        return(
                            <StyledTableRow key={index}>
                                <StyledTableCell>{row.id}</StyledTableCell>
                                <StyledTableCell>{row.firstname}</StyledTableCell>
                                <StyledTableCell>{row.lastname}</StyledTableCell>
                                <StyledTableCell>{row.country}</StyledTableCell>
                                <StyledTableCell>{row.subject}</StyledTableCell>
                                <StyledTableCell>{row.customerType}</StyledTableCell>
                                <StyledTableCell>{row.registerStatus}</StyledTableCell>
                                <StyledTableCell><Button variant="contained" onClick={() => handleButtonEdit(row)}>Sửa</Button>
                                <Button sx={{
                                    ml: 1,
                                    backgroundColor: red[700],
                                    '&:hover': {
                                        bgcolor: red[900]
                                    }
                                }}
                                variant="contained" 
                                onClick={()=>handleButtonDelete(row)}
                                >Xóa</Button></StyledTableCell>
                            </StyledTableRow>
                        )
                    })}
                </TableBody>
            </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />

            {/*========QUẢN LÝ MODAL CREATE====== */}
            <Modal
                open={openCreate}
                onClose={handleCloseCreate}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Thông tin người đăng ký
                </Typography>
                <TextField onChange={onChangeFirstName} fullWidth sx={{mt: 2}} label={'First name'}/>
                <TextField onChange={onChangeLastName} fullWidth sx={{mt: 2}} label={'Last name'}/>
                <TextField onChange={onChangeSubject} fullWidth sx={{mt: 2}} label={'Subject'}/>
                <TextField 
                value={country}
                onChange={onChangeCountry} 
                select 
                helperText='Vui lòng chọn quốc gia' 
                fullWidth 
                sx={{mt: 2}} 
                label={'Country'}
                >
                    {countriesArr.map((country, index) => (
                        <MenuItem key={index} value={country.value}>
                            {country.label}
                        </MenuItem>
                    ))}
                </TextField>
                <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: green[900],
                            '&:hover': {
                                bgcolor: green[600]
                            },
                            mr: 8
                        }}
                        onClick={handleConfirmCreate}
                        >
                            Xác nhận
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseCreate}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {/*========END-QUẢN LÝ MODAL CREATE====== */}

            {/*========QUẢN LÝ MODAL EDIT====== */}
            <Modal
                open={openEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                id="U - Update"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Chi tiết User
                </Typography>
                <TextField
                    value={selectRow.firstname}
                    onChange={(e) => {
                        const updateFirstName = {...selectRow, firstname: e.target.value}
                        setSelectRow(updateFirstName)
                    }}
                    label={'First Name'}
                    fullWidth
                    sx={{
                        mt: 2
                    }}
                />
                <TextField
                    label={'Last Name'}
                    value={selectRow.lastname}
                    onChange={(e) => {
                        const updateLastName = {...selectRow, lastname: e.target.value}
                        setSelectRow(updateLastName)
                    }}
                    fullWidth
                    sx={{
                        mt: 2
                    }}
                />
                <TextField
                    label={'Subject'}
                    value={selectRow.subject}
                    onChange={(e) => {
                        const updateSubject = {...selectRow, subject: e.target.value}
                        setSelectRow(updateSubject)
                    }}
                    fullWidth
                    sx={{
                        mt: 2
                    }}
                />
                <TextField
                    label={'Country'}
                    value={selectRow.country}
                    onChange={(e) => {
                        const updateCountry = {...selectRow, country: e.target.value}
                        setSelectRow(updateCountry)
                    }}
                    fullWidth
                    sx={{
                        mt: 2
                    }}
                />
                <TextField
                    label={'Customer Type'}
                    value={selectRow.customerType || ''}
                    onChange={(e) => {
                        const updateCustomerType = {...selectRow, customerType: e.target.value}
                        setSelectRow(updateCustomerType)
                    }}
                    fullWidth
                    select
                    sx={{
                        mt: 2
                    }}
                >
                   {customerTypeArr.map((value, index) => (
                    <MenuItem key={index} value={value.value}>
                        {value.label}
                    </MenuItem>
                   ))} 
                </TextField>
                <TextField
                    label={'Register Status'}
                    fullWidth
                    select
                    value={selectRow.registerStatus || ''}
                    onChange={(e) => {
                        const updateRegisterStatus = {...selectRow, registerStatus: e.target.value}
                        setSelectRow(updateRegisterStatus)
                    }}
                    sx={{
                        mt: 2
                    }}
                >
                    {registerStatusArr.map((register, index) => (
                        <MenuItem key={index} value={register.value}>
                            {register.label}
                        </MenuItem>
                    ))}
                </TextField>
                <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: blue[900],
                            '&:hover': {
                                bgcolor: blue[600]
                            },
                            mr: 8
                        }}
                            onClick={() => handleUpdateUser(selectRow)}
                        >
                            Update User
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseEdit}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {/*========END-QUẢN LÝ MODAL EDIT====== */}


            {/*========QUẢN LÝ MODAL DELETE====== */}
            <Modal
            open={openDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography variant="h6" align="center">
                        {'Xác nhận muốn xóa thông tin này'}
                    </Typography>
                    <ButtonGroup sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Button variant="contained" sx={{
                            backgroundColor: red[600],
                            '&:hover': {
                                bgcolor: red[900]
                            },
                            mr: 8
                        }}
                            onClick={() => handleDeleteUser(selectRow)}
                            >
                            Xác nhận
                        </Button>
                        <Button variant="contained" sx={{
                            backgroundColor: grey[300],
                            color: grey[900],
                            '&:hover': {
                                bgcolor: grey[400]
                            }
                        }}
                        onClick={handleCloseDelete}
                        >
                            Hủy bỏ
                        </Button>
                    </ButtonGroup>
                </Box>
            </Modal>
            {/*========END-QUẢN LÝ MODAL DELETE====== */}
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
                    {severity === 'success' ? <AlertTitle>Success</AlertTitle>: <AlertTitle>Error</AlertTitle>}
                    {message}
                </Alert>
            </Snackbar>
        </Container>
    )
}
export default DataTableUser;